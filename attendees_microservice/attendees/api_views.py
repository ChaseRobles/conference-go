from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Attendee, ConferenceVO
# from events.models import Conference
import json
from common.json import ModelEncoder


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]



class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name",
                  "email",
                  ]



class AttendeeDetailsEncoder(ModelEncoder):
    model=Attendee
    properties = [
                "email",
                "name",
                "company_name",
                "created",
    ]
    def get_extra_data(self, o):
        return {
            "conference_name": o.conference.name,
            "href": o.conference.get_api_url(),
        }

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":

        attendees_list = []
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        # attendees = Attendee.objects.all()
        for attendee in attendees:
            attendees_list.append({
                    "name": attendee.name,
                    "href": attendee.get_api_url(),
                })
        return JsonResponse({'attendees': attendees_list})

    else:
        content = json.loads(request.body)
    try:
        conference_href = f'/api/conferences/{conference_vo_id}/'
        conference = ConferenceVO.objects.get(import_href=conference_href)
        content["conference"] = conference
    except ConferenceVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailsEncoder,
        safe=False,
    )




require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    if request.method == 'GET':
        a = Attendee.objects.get(id=id)
        return JsonResponse(
            a,
            encoder=AttendeeDetailsEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailsEncoder,
            safe=False,
        )
