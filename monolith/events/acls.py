from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
#OPEN_WEATHER_API_KEY

def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    # url = f'https://api.pexels.com/v1/search?query={city},%20{state}&per_page=1'
    url = 'https://api.pexels.com/v1/search'
    headers = {'Authorization': PEXELS_API_KEY}
    params = {
        'query': f'{city}, {state}',
        'per_page': "1"
              }
    r = requests.get(url, headers=headers, params=params)
    content = r.json()
    x = { 'picture_url': content['photos'][0]['src']['original']}
    return x



# def get_weather_data(city, state):
#     # Create the URL for the geocoding API with the city and state
#     url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit={1}&appid={OPEN_WEATHER_API_KEY}'
#     # Make the request
#     r = requests.get(url)
#     re = r.json()
#     # Parse the JSON response
#     # Get the latitude and longitude from the response
#     lat = re[0]["lat"]
#     lon = re[0]["lon"]
#     # Create the URL for the current weather API with the latitude
#     url2 = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}'
#     #   and longitude
#     # Make the request
#     request = requests.get(url2)
#     # Parse the JSON response
#     content = request.json()
#     # Get the main temperature and the weather's description and put
#     x= {
#         "temperature": content['main']['temp'],
#         'description': content['weather'][0]['description'],
#     }
#     return x
#     #   them in a dictionary
#     # Return the dictionary
def get_weather_data(city, state):
    # Use the Open Weather API
    # Create the URL for the geocoding API with the city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    r = requests.get(url)
    # Parse the JSON response
    context = r.json()
    print('this is context',r)
    # Get the latitude and longitude from the response
    lat = context[0]["lat"]
    lon = context[0]["lon"]
    weather = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather)
    content = weather_response.json()
    y = ((content["main"]["temp"]) - 273.15) * (9/5) + 32
    d = {
        "main temperature": f'{int(y)}F',
        "weather description": content["weather"][0]["description"],
    }
    return d
