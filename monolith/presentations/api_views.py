from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
import json


@classmethod
def create(cls, **kwargs):
    kwargs["status"] = Status.objects.get(name="SUBMITTED")
    presentation = cls(**kwargs)
    presentation.save()
    return presentation



class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]
    def get_extra_data(self, o):
        return { 'status': o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

    def get_extra_data(self, o):
        return { "status": o.status.name }

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.get(conference_id=conference_id)
        return JsonResponse(
            {"presentations": presentation},
            encoder=PresentationListEncoder,
        )
    else:pass



def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
